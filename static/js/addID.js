class addId extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    addIDtoEachElement(content);
  }
}

Paged.registerHandlers(addId);


function addIDtoEachElement(content) {
  let tags = ["figure", "figcaption", "img", "ol", "ul", "li", "p", "img", "table", "h1", "h2", "h3", "h4", "div", "aside"];
  tags.forEach( tag => {
    content.querySelectorAll(tag).forEach((el, index) => {
      if (!el.id) {
        el.id = `el-${el.tagName.toLowerCase()}-${index}`;
      }
    });
  })
}

function addLogo(content) {
  content.querySelector("section").insertAdjacentHTML('afterbegin', `<img class="logo" src="images/uploads/kotahiJournalLogo.svg" alt="logo" />`)
}

// function rotateLargeImages(content){
//   content.querySelectorAll('img').forEach(image =>{
//     const width =image.style.width, height=image.style.height;
//     if( (width/height >= 1.4) && (width > 1000)){
//       image.style.transform = "rotate(90deg)";
//     }
//   })
// }
// editiora move cst   
//
//
class editoriaClean extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
          addLogo(content);
          // rotateLargeImages(content)
    // content.querySelectorAll("section").forEach(section => {
    //   moveInHeader(section, [".cst", ".author"]);
    // })
  }

}

Paged.registerHandlers(editoriaClean);

function moveInHeader(content, els) {
  // for (let item of els) {
  //   console.log(item)
  //   let moveables = content.querySelectorAll(item)
  //     moveables.forEach(moving => {
  //     if (moving && moving.closest('section').querySelector('header')) {
  //       moving.closest('section').querySelector('header').insertAdjacentElement('beforeend', moving);
  //     }
  //   })
  // }
}
