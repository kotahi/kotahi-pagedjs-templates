const { DateTime } = require("luxon");

module.exports = function (eleventyConfig) {




  eleventyConfig.addPassthroughCopy({ "static/css": "/css" });
  eleventyConfig.addPassthroughCopy({ "static/fonts": "/fonts" });
  eleventyConfig.addPassthroughCopy({ "static/js": "/js" });
  eleventyConfig.addPassthroughCopy({ "static/images/": "/images/" });
  eleventyConfig.addPassthroughCopy({ "static/outputs": "/outputs" });






  // \get the date with luxon (for all date)
  eleventyConfig.addFilter("postDate", (dateObj) => {
    let date = new Date(dateObj)
    return DateTime.fromJSDate(date).toLocaleString(DateTime.DATE_MED);
  });








  eleventyConfig.addFilter("filterHTML",str => str.replace( /(<([^>]+)>)/ig, ' '))

  eleventyConfig.addFilter("slugify", function (str) {
    return slugify(str, {
      lower: true,
      replacement: "-",
      remove: /[*+~.·,()'"`´%!?¿:@]/g
    });
  });


  // folder structures
  // -----------------------------------------------------------------------------
  // content, data and layouts comes from the src folders
  // output goes to public (for gitlab ci/cd)
  // -----------------------------------------------------------------------------
  return {
    dir: {
      input: "src",
      output: "public",
      includes: "layouts",
      data: "data",
    },
  };
};


function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

