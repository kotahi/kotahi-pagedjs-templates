---------------------
layout components 
-----------------------
-----------------------

All pages
------------
1. Thick border on the left
2. right: around 1/4 of left
3. top/ bottom almost equal
4. top-left margin contains basic elife logo with research level(access from objectType) as subscript
5. Bottom-left margin contains copyright + elife + year; article number.  link to the document
6. bottom-right margin contains current page of total pages
7. 5 + 6 forms the footer with a border on top
8. the h2 tags also have a border-top
9. figures and tables have a border bottom
10. figures stretch through the left margin. On the places where there are figure left margin = right margin
11. no column divides for tables
12. top-right contains tags seperated by |


First page
--------------
1. Has addition margin space on top 
2. top-left corner contains proper colored elife logo
3. top right contains research level(access from objectType)  | open access logo | CC logo
4. below the title, all the authors are given, with super-scripts after the author name(numbers for affiliations and a few symbols)
5. Below the authors, affiliations are given with superscripts(numbers) before the affiliation name
6. The left-bottom margin contains
  -- meaning of the symbol superscripts with superscripts at the start.
  -- competing interest (access from conflictOfInterest)
  -- Funding table page link
  -- Received, Accepted, and published dates
  -- Reviewing editor's details
  -- Copyright details


Changes on JSON
----------------
1. Convert author.affiliations from string to array
2. added missing components in missing property
3. change the name of parsedSubmission.content to parsedSubmission.topic(tagList mentioned earlier in missing)
4. Change location of additional information in JSON (remove from meta.source) - editorsEvaluation(add in parsedSubmission.editorsEvaluation), copyright(add in meta.permissions), authorContribution(separate for each author, that is, directly add under parsedSubmission.authors), seniorEditors(add in parsedSubmission.seniorEditors), reviewers(add in parsedSubmission.reviewers), reviewingEditoors(add in parsedSubmission.reviewingEditors), acknowledgements(add in parsedSubmission.acknowledgements), ethics(add in parsedSubmission.ethics), publicationHistory(added in parsedSubmission.publicationHistory)

6. author contributions, individual Competing Interest declaration should be part of authors. Old version, Author data is getting repeated, check - `meta.source` and `parsedSubmission.authors` - put it all into `parsedSubmission.authors` 

7. there is a `history` property in `meta`. Was that supposed to be for publication history. If yes, add publicationHistory either in  `parsedSubmission.publicationHistory` or `meta.history`
8. add `equalContributor1` and `equalContributor2` in `parsedSubmission.authors` 
9. add `publisherMetaData` in meta

Variables in CSS
------------------
1. things that don't cascade
2. things that may change
3. naming: property-childProperty e.g. font-heading


